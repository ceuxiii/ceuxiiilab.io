#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Comissão Organizadora'
SITENAME = 'XIII CEU'
# SITEURL = 'ceuxiii.gitlab.io'

THEME = 'theme'
PYGMENTS_STYLE = 'monokai'
HEADER_COVER = 'theme/header.jpg'
HEADER_TABLET = 'theme/header.jpg'
PATH = 'content'
FACEBOOK_URL = 'https://www.facebook.com/XIII-Congresso-dos-Estudantes-da-Unicamp-100115441373059'

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

MENUITEMS = (
    ('Sobre', '/pages/sobre.html'),
    ('Contato', '/pages/contato.html'),
    ('Cronograma', '/pages/cronograma.html'),
    ('Convidados', '/pages/convidados.html'),
    ('Teses', '/category/teses.html'),
    ('Contribuições', '/category/contribuicoes.html')
)

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = 'pt_br'

PLUGINS = [
   'foo',
   'rmd_reader',
   'bar',
]

MARKUP = ('md', 'ipynb', 'rmd')

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# Social widget

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
