title: Tese MUP
category: teses
show: yes
date: 2019-10-1
author: CO

---

__Unidade para resistir, ousadia para avançar! Teses do Movimento por uma Universidade Popular (MUP) ao XIII Congresso dos Estudantes da Unicamp__

__i. Conjuntura__

O Brasil vive hoje uma profunda crise política e econômica. O governo Bolsonaro transparece cada vez mais seu caráter antinacional, antipopular e antidemocrático, fazendo avançar de vento em popa as diversas medidas neoliberais que já vínhamos sofrendo nos últimos anos. A privatização e a entrega de nosso patrimônio aos interesses do capital internacional, os crimes ambientais para a expansão do latifúndio, a reforma da previdência e os demais ataques e perseguições aos nossos direitos mais básicos constituem o projeto de espoliação nacional atualmente em curso. No caso do ensino superior, o ​Future-se ​é apresentado como ponta de lança do ataque da classe dominante à educação pública.

Dia após dia, assistimos a diversos cortes de verba sendo planejados e efetivados, ao ponto de colocar em risco o funcionamento de muitas universidades federais pelo país. A ofensiva dos interesses empresariais sobre a educação materializa-se com o corte nos orçamentos de instituições de financiamento à pesquisa, como CNPq e CAPES, e com o projeto ​Future-se​, que visa uma verdadeira privatização do ensino superior, restringindo ainda mais a extensão, o ensino e a pesquisa nas universidades públicas ​— as responsáveis por 95% de toda a ciência nacional ​— e aprofundando, assim, a nossa dependência científico-tecnológica e econômica aos países centrais do capitalismo. Simultaneamente ao processo de sucateamento da educação pública, o governo federal efetua diversos ataques à autonomia universitária, de modo a nomear interventores para a administração de universidades federais, passando por cima das eleições realizadas nesses espaços.

A nível estadual a situação não é muito diferente: a base aliada de João Dória deu início à CPI das Universidades Públicas. Essa medida é mais uma forma de perseguição ideológica e de tentar minar a autonomia universitária. A CPI não ocorre por acaso ou como um meio de “poupar gastos”. Muito pelo contrário: é resultado direto de um projeto de educação que está sendo colocado, à força, para nós! Um projeto pautado na precarização e tecnicização do ensino, contra toda promoção do pensamento crítico e apenas voltado para a formação de uma massa de força de trabalho barata a ser inserida no mercado. Na mesma linha, à despeito do discurso da reitoria “em defesa da universidade pública”, foi recentemente deliberada no CONSU da Unicamp a adoção dos fundos patrimoniais — mais uma forma de subordinar nossas pesquisas aos interesses da classe dominante!

O objetivo desses ataques já nos é claro: destruir a educação pública e enriquecer os grandes empresários do ensino privado. São medidas que não apenas nos retiram o direito à educação, mas também comprometem a pesquisa e o desenvolvimento tecnológico, corroendo ainda mais a nossa soberania nacional e aprofundando a condição de subalternidade do nosso país. Mais uma vez, Bolsonaro e Dória demonstram como estão alinhados aos interesses não
apenas da classe dominante, mas também das potências imperialistas que nos subjugam.

Os estudantes, porém, não têm se calado frente a esses ataques. Vimos ao longo dos últimos meses diversos atos e atividades construídos pelo movimento estudantil e convocados por suas entidades, como a União Nacional dos Estudantes (UNE) e a União Estadual dos Estudantes de São Paulo (UEE-SP), em resposta ao projeto antinacional, antipopular e antidemocrático em curso. Vimos ao redor de todo o país, em diversas cidades, escolas e universidades, o movimento estudantil indo às ruas e dando as caras para mostrar que o povo está insatisfeito e que não iremos aceitar a privatização da educação! Queremos uma educação voltada às necessidades de nossa população e comprometida com um projeto de desenvolvimento nacional soberano! Para defender os interesses de nosso povo, propomos a construção de um novo projeto de universidade; que ela esteja alinhada aos interesses de nossa classe: uma Universidade Popular!

__ii. O papel da Universidade hoje e a Universidade Popular que queremos!__

É após o crescimento e a consolidação, nos últimos vinte anos, de monopólios como Kroton, Estácio e outros conglomerados bilionários que o setor privado de educação se coloca como projeto educacional de contraposição ao ensino superior público.

Nas últimas décadas, enquanto se consolidavam como protagonistas o agronegócio e o setor financeiro, o Brasil passou por um processo de desindustrialização. Nesse contexto, é ínfima a necessidade do desenvolvimento de pesquisas científicas e de produção tecnológica por parte do capital privado, uma vez que, quando é necessário qualquer avanço de produtividade, são importadas novas tecnologias dos países centrais. Isso se torna visível se percebemos como grande parte de nossa pesquisa é destinada às demandas e paradigmas alheios à nossa população: por exemplo, ao invés de concentrar nossos esforços em uma pesquisa que avance a soberania nacional e retome a industrialização de nossa economia à serviço das necessidades da classe trabalhadora ​— inclusive com a produção nacional de colhedoras, tratores e afins ​— são desenvolvidos melhoramentos e adaptações para o maquinário agrícola de uso típico em climas mais amenos. Embora muitos de nossos pesquisadores sejam referenciados entre os melhores do mundo em várias áreas do conhecimento, nossa pesquisa muitas vezes não serve ao nosso povo.

Porém, se cada vez mais nossas universidades públicas estão alheias à nossa população, sem elas é impossível lutarmos por uma educação universal, com ensino crítico e com uma produção científica voltada aos desafios de nossa sociedade. É apenas a partir de nossas pesquisas que podemos pensar e avançar sobre as dificuldades do Brasil e do povo brasileiro, de nossa relação com o resto do mundo, de nossa autonomia nacional e assim romper as nossas amarras do imperialismo. ​**Portanto, defendemos a nossa pesquisa autônoma, através do financiamento público, com todas as nossas energias!** ​Esse é o primeiro passo, que é necessário, mas não suficiente. **​Também precisamos defender uma pesquisa que sirva ao Brasil e à nossa população, de modo que ela se sobreponha àquela que atualmente serve aos interesses privados.**

Como realizar pesquisa para a população quando nossas universidades são, em grande parte, míopes ao resto de Brasil? **​A implementação tanto das cotas raciais e sociais quanto do vestibular indígena constituem apenas um primeiro passo.** Porém, para integrar a universidade à sociedade é necessário levar o conhecimento aqui produzido para onde está a população. **​É necessário ampliar os cursos de extensão e torná-los gratuitos e de fácil acesso, diferentemente do que predomina hoje na Unicamp​;** é fundamental que essa extensão seja uma via de mão dupla: que também o conhecimento popular seja colocado para dentro da universidade.

Sem estudantes, não existe ensino nem pesquisa. Precisamos pensar em como nós, estudantes, vivemos e sobrevivemos em nossa universidade. Aliado aos insuficientes programas de permanência estudantil, como as escassas bolsas sociais e vagas na moradia estudantil, um fator que impede a permanência na universidade é a condição de nossa saúde mental, muitas vezes esmagada por grades curriculares absurdas, perseguição acadêmica e pouca, quando alguma, assistência psicológica gratuita no campus. **​Os estudantes precisam viver além do conteúdo da sala de aula. ​Por esse motivo, lutamos por nossa moradia e por um programa de bolsas que garanta a nossa permanência, por grades curriculares que permitam o desenvolvimento profissional e pessoal do estudante; lutamos contra o assédio e a perseguição pelo corpo docente, bem como exigimos um atendimento psicológico que nos alcance. A juventude exige um ambiente universitário mais humano.**

Nós defendemos uma universidade em que não apenas a população esteja nas salas e laboratórios, mas que também a produção de conhecimento, o desenvolvimento de pesquisa e a inovação tecnológica estejam vinculados à necessidade de superar os principais obstáculos ao livre desenvolvimento de todos e de cada um, denunciando as mazelas geradas pelo capitalismo e expondo as contradições de sua ideologia. A formação em nível técnico, de graduação e pós-graduação, deve atender não a demanda pela administração e desenvolvimento da sociabilidade burguesa, mas a necessidade de formação de profissionais tão tecnicamente preparados quanto críticos, comprometidos com um projeto emancipatório. Uma universidade em que o conhecimento produzido não fique restrito aos seus muros, mas que ele esteja em constante integração com o povo. **​Além disso, uma Universidade Popular é, necessariamente, uma universidade democrática; para isso, é preciso que as instâncias deliberativas não privilegiem uma categoria acima das outras, que nos seja permitido escolher diretamente os rumos de nossa universidade e que nos seja garantida a nossa autonomia financeira.**

__iii. A importância do Congresso dos Estudantes da Unicamp__

Temos no movimento estudantil brasileiro um histórico e importante campo de luta de massas, presente em diversos episódios importantes de nosso passado. Na Unicamp, a mobilização estudantil foi responsável por uma série de conquistas, desde a construção da moradia estudantil no final dos anos 80 até outras mais recentes: com a greve de 2016, por exemplo, conquistamos as cotas étnico-raciais e o vestibular indígena, ambos efetivados a
partir deste ano de 2019.

Não apenas à luz da conjuntura atual, a importância histórica do movimento estudantil é
retomada quando relembramos o nosso histórico de lutas, que em extenso período atuou como um setor fundamental da sociedade brasileira, agregando grandes esforços em prol da classe trabalhadora. Nesse contexto, o XIII Congresso dos Estudantes da Unicamp (CEU) se apresenta como um importante espaço de mobilização, debates e construção de uma nova direção para nossas lutas. Para que se volte à tona a necessidade de construção não apenas de um movimento estudantil massificado e próximo ao corpo discente e às nossas demandas, mas também de um movimento estudantil que parta para o enfrentamento pelo projeto de país e de sociedade que defendemos. ​**Disputando também um projeto de universidade, reafirmando que somente a “universidade pública, gratuita e de qualidade” não nos basta e empenhando esforços, junto aos movimentos sociais e com ousadia, visamos a construção e efetivação de uma Universidade Popular​.**

O Congresso dos Estudantes, que ocorre entre os dias 14 e 19 de outubro e reúne estudantes de todos os cursos da Unicamp, será fundamental para que se sintetize, através do diálogo e do debate sobre os contextos que nos permeiam, a construção de uma nova proposição de enfrentamento à ofensiva da classe dominante. Unidade para resistir, ousadia para avançar! É hora de lutar e criar uma Universidade Popular!




Assinam essa tese:

Angelo Gabriel Uehara Ardonde, 164239 <br>
Aline Damasceno Brancacci, 165350 <br>
Anna Emília Limongi de Vasconcelos Paiva, 164267 <br>
Ana Flávia Evangelista, 193883 <br>
Ana Helena Alves Reis da Silva, 163836 <br>
Arthur Gabriel do Nascimento, 194379 <br>
Beatriz Tierno Waberski, 213645 <br>
Bruno Santos Telles Pagotto, 154891 <br>
Caio Rosalles Seccoli Xavier de Oliveira, 232620 <br>
Daniel da Silva Sarlo Vilela, 169414 <br>
Fernando Savella, 167551 <br>
Fernando Simões Guimarães Pinto Nazário, 215932 <br>
Gabriel Zupirolli de Almeida, 197580 <br>
Gabriel Silva Gonçalves, 235042 <br>
Guilherme Yoshio Ágata Domingues, 217350 <br>
João Caetano Gomes Viana, 218570 <br>
Juan Gansauskas Galvez, 176729 <br>
Júlia Cristina Zebini, 238135 <br>
Larissa Medeiros Figueredo, 156156 <br>
Laura Wachter Hara, 239042 <br>
Lucas Bernardes de Almeida, 201796 <br>
Monalisa Roberto da Silva, 242174 <br>
Mariana Silveira Marques, 183827 <br>
Mateus Prado Garcia, 184383 <br>
Nikolas Maciel Carneiro, 156884 <br>
Rafael Rocha Novais, 243465 <br>
Raul Gustavo Missias Franco, 176471 <br>
Tainá Maria Vieira Da Rocha Silva, 205926 <br>
Thiago Vieira Calile Costa, 206286 <br>
Victor da Costa Ramos, 187868 <br>
Vinícius Camilotti Berni, 245262 <br>
