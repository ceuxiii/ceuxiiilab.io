title: O que é um delegado?
author: Comissão Organizadora
date: 5-09-3019
category: sobre
show: yes

---
Os delegados são estudantes que as Unidades escolhem para serem representantes durante o CEU. Cada instituto/faculdade tem direito a 1 (um) delegado para cada 20 (vinte) estudantes regularmente matriculados na graduação. Se houver mais interessados em ser delegados do que vagas disponíveis, a Unidade precisa realizar um processo de escolha de delegados. Essa escolha pode ser feita de três formas: eleição em assembleia, eleição em urnas ou por indicação referendada em reunião aberta e amplamente divulgada pelo Centro Acadêmico. Caso o seu Centro Acadêmico não esteja divulgando ou construindo o processo de escolha de delegados, converse com a gestão do DCE ou com a Comissão Organizadora, que faremos o possível para ajudá-los a encaminhar isso.
