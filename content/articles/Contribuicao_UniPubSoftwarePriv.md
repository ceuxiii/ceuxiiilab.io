title: Universidade Pública, Software Privado?
category: contribuicoes
show: yes
date: 2019-10-3
author: CO

---

Hoje em dia caixas de e-mail não estouram, o Photos libera upload sem limite no Android, telefones chineses são baratos, populares e ligados a servidores daquele país; damos centenas de likes ao dia se deixar. Na banalidade da vida individual, em geral nos sujeitamos aos “termos de uso” e trocamos a privacidade pela praticidade e prazer. O que acontece quando a universidade pública faz o mesmo?

**Entrada compulsória de empresas**<br>
Não é incomum a entrada de empresas ser imposta à vida estudantil. Em 2016 a reitoria vinculou ao Santander um RA-de-débito compulsório para os estudantes. Dezenas de funcionários do banco assediavam estudantes pelo campus a abrirem suas contas. O banco se sentiu devidamente expulso após um grande pula-catraca no RU.

Agora, tivemos nossos e-mails movidos para dentro dos servidores do Google. O mxtoolbox.com demonstra que os domínios unicamp.br, usp.br e unesp.br vão direto pra lá. Assim como com o Santander, não houve debate na comunidade sobre essas “soluções gratuitas”, que terceirizam uma parte importante da infraestrutura tecnológica da universidade para uma empresa.

Óbvio que não é por caridade. É extremamente valioso, para uma empresa que faz dinheiro sobre bilhões de dados por segundo, que esses dados cheguem catalogados nos seus devidos lugares – por exemplo, no seu perfil. Você provavelmente vai querer logar no email DAC no dispositivo onde já estão suas contas pessoais, certo? Bingo! Mais um dado pro Google.

Mais do que isso, o tipo de dado dos usuários de uma universidade é muito mais relevante. Além de ser em geral um setor de consumo privilegiado da sociedade, também contém informações sensíveis de patentes e pesquisas em desenvolvimento, sobretudo de professores e pós-graduandos que têm mais probabilidade de utilizar um endereço só para comunicações acadêmicas. Em alguns casos é obrigatório utilizá-los – por exemplo, para registrar-se em sites e ferramentas digitais que oferecem descontos a estudantes. Bingo de novo!

**Mais do que o e-mail**<br>
O CECOM passou nos últimos anos a utilizar o software de gestão empresarial nacional Senior, cujos contratos raramente ficam abaixo dos R$ 500 mil. No site da empresa não há sistema de gestão de hospitais no portfolio, e a versão que roda no CECOM é uma “geralzona”. Isso amplia a possibilidade de gastar ainda mais dinheiro com implementação de funções específicas ao trabalho hospitalar.

**Universidade para si própria**<br>
A relação da universidade com o software já foi outra. Diante de uma demanda importante, como a organização dos acervos de bibliotecas, profissionais da Unicamp e do ITA criaram o Sophia Biblioteca. O negócio é tão bom que serve 600 instituições hoje, aqui e lá fora. Porém, o que poderia ter se tornado software livre, disponibilizado gratuitamente com retorno inesgotável à sociedade, desde 1993 se tornou a Prima Software, empresa-membro do sindicato patronal de TI.

Os próprios sistemas da DAC são outro exemplo. São milhares de linhas de código rodando em infraestrutura pública, ano após ano processando todos os registros acadêmicos. O fato de haver tantos problemas nada mais é do que retrato do desinvestimento e desfalque no quadro de funcionários. Quem sabe quando der pau de vez não se abra “oportunidade” para a “inovação” e contratemos, pagando com mais do que dados, outra “solução” privada. Para defender a Universidade Pública devemos adicionar, às nossas bandeiras, a defesa intransigente do software livre na universidade!
