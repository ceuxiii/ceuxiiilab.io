title: Tese Pra Poder Contra-Atacar: Que os Capitalistas paguem pela crise!
category: teses
show: yes
date: 2019-10-1
author: CO
---

**PRA PODER CONTRA-ATACAR: QUE OS CAPITALISTAS PAGUEM PELA CRISE!**

**Sair da rotina para enfrentar Bolsonaro e Weintraub: em defesa de entidades vivas, militantes e pela base a serviço de um programa anti-imperialista e anticapitalista**

_Tese assinada pela juventude Faísca, Pão e Rosas, Quilombo Vermelho e estudantes independentes._

“Corra, camarada, o velho mundo está atrás de você”, pichavam jovens franceses que desafiavam o poder em Maio de 68. No Brasil atual, marcado por uma extrema direita que quer nosso suor e sangue, a juventude saiu às ruas se colocando como a principal oposição a Bolsonaro e carregando as experiências de uma nova geração que participou de atos massivos, ocupações de escola e de universidade em resposta à crise capitalista. É nossa tarefa nos contrapor ao “velho mundo” e, para isso, devemos tomar nas mãos as ferramentas que historicamente foram construídas pela juventude inspirada nas lutas de trabalhadores, fazendo delas fortalezas para nos organizar: as entidades estudantis.

Para cada Weintraub que quer nos robotizar para as empresas, que sejamos milhares de jovens lado a lado aos trabalhadores, indígenas, sem-terra e sem-teto, produzindo conhecimento para atender às suas demandas. Para cada Damares, que ameaça a vida das mulheres e LGBTs, que mostremos orgulhosamente toda subversão do corpo, da sexualidade, e que cada jovem negro possa erguer seu black por todas as Agathas. Para cada Bolsonaro que despeja a conta da crise nas nossas costas, que construamos entidades vivas, militantes, que organizem a luta pela base e façam a extrema direita tremer com a juventude aliada aos trabalhadores. Nada menos que isso. Se eles têm um projeto radical anti-operário, misógino, racista e LGBTfóbico, é com a tarefa de nos organizar para responder com um projeto radical a serviço dos oprimidos e explorados que queremos ir ao XIII Congresso dos Estudantes da Unicamp.

O CEU deve ser um espaço para os estudantes que não aceitam o projeto de universidades-empresa, do desemprego, do trabalho precário do Rappi, do assassinato dos negros e indígenas, de trabalhar até morrer sem direitos. O CEU deve responder: como transformar a potência da juventude organizada, que sacudiu o Brasil em maio, em uma força imparável capaz de movimentar todos os atacados por Bolsonaro? Para essa resposta, o movimento estudantil da Unicamp sempre esteve à frente contra os ataques à educação, mesmo quando vinham do PT, lutou contra o golpe e arrancou, junto aos movimentos sociais, as cotas e o vestibular indígena. Agora, mais do que nunca, essas conquistas estão ameaçadas por Dória e Bolsonaro e temos de estar à frente de responder.

**Unificar a juventude nacionalmente: o futuro é nosso e não deles!**

Por isso, batalhamos desde o início do ano por um CEU pela base, bem divulgado, com assembleias e reuniões nos cursos para que os estudantes sejam parte ativa da discussão, elegendo seus representantes. Não queremos um espaço formal. Se a UNE, dirigida por PT e PCdoB, aposta em atos dispersos para cumprir calendário, enquanto negociam por cima com Maia, qual força dos de baixo devemos construir? Para estar à altura de enfrentar Bolsonaro, nossas entidades devem ser ferramentas dar grandes respostas, e isso passa por batalhar pela auto-organização, com cada estudante sendo sujeito de pensar o programa e estratégia contra nossos inimigos, e não que as decisões sejam tomadas nas reuniões de cúpulas de poucos.

O ano começou com cortes de 30% nas universidades, Bolsonaro e seus generais intervieram na escolha de reitores, e estão aprovando a Reforma da Previdência. Anunciaram uma nova reforma trabalhista (MP881), o Future-se e cortes nas bolsas de pesquisa. Somos jovens de uma universidade que, apesar de atacada em uma das maiores pós-graduações, é das mais preservadas frente ao que querem Weintraub e Bolsonaro. Entretanto, não temos dúvida de que seu projeto privatista quer alcançar o conjunto das universidades públicas do país e devemos nos unificar com as federais, os pesquisadores, futuros professores, e as demais estaduais paulistas.

Esse projeto privatizante quer submeter a ciência ainda mais aos grandes empresários, especialmente do agronegócio que faz a Amazônia arder em chamas, e teve caminho aberto pela conciliação de classes do PT. Enquanto Bolsonaro quer tornar o Brasil uma “fazenda do mundo”, ajoelhado a Trump, outros países imperialistas como a França e Alemanha aparecem como “defensores do meio ambiente”, quando na verdade têm o mesmo objetivo de degradação do planeta. A saída para a crise ecológica e da Amazônia deve ser anticapitalista e anti-imperialista, e o ensino e a pesquisa cumprem nisso um importante papel. Contra os ataques na educação e na saúde, lutamos pelo não pagamento da dívida pública.

O projeto de país em curso é fruto do golpe institucional, que tem Marielle como uma ferida aberta, e articulado ao autoritarismo judiciário de Moro, da Lava Jato e do STF, prendeu Lula. Por isso somos contra a prisão arbitrária de Lula sem prestar nenhum apoio ao PT e há 1 ano e meio sem Marielle lutamos por uma investigação independente para chegar aos mandantes do crime.

**Abaixo a CPI das Estaduais e a repressão às festas: ocupar a universidade com política e cultura**

Esse Judiciário, na Unicamp, censurou aulas sobre o golpe e, com sua liminar, avançou contra as festas no campus. A censura que eles nos impõem tem um conteúdo: reprimir a juventude e desmoralizar nossos espaços, como se fossem a “balbúrdia” de Bolsonaro. Estão à serviço dos que desprezam as Humanas, atacam o marxismo e negam a ciência e as mudanças climáticas para avançar contra a autonomia universitária. É para calar o pensamento crítico e os que lutam que as universidades estão sob ataque.

Não aceitaremos nenhum tipo de censura e repressão, Bolsonaro ataca a UNE porque sabe da força que essa burocracia busca conter. Na Unicamp é preciso ocupar nossos espaços com política, arte e cultura para dizer que nenhuma bienal, exposição ou expressão da juventude será calada e que defendemos a autonomia universitária. Por isso, chamamos as entidades estudantis, como o DCE, a não abrirem mão desse combate e levarem adiante espaços de vivência, festas e festivais contra a repressão e a política de esvaziamento da universidade em prol do produtivismo.

No estado de SP, Dória sinalizou ao reacionarismo de Bolsonaro contra a suposta “doutrinação” nas escolas recolhendo apostilas sobre identidade de gênero e sinaliza aos liberais da educação para transformar o ensino em formação ao trabalho precário, como é o Novotec que está implementando a Reforma do Ensino Médio em SP. Seu governo, junto a deputados reacionários do PSL, também avança com a CPI das estaduais e abre caminho ao desmonte e privatização.

A reitoria da Unicamp se coloca como defensora da autonomia universitária e diz que vai arcar emergencialmente com parte das bolsas cortadas, mas se apóia na liminar da justiça contra as festas para nos ameaçar, não ampliará a moradia, precariza as condições de trabalho e esteve junto a Bolsonaro na defesa dos fundos patrimoniais. A partir do Conselho Universitário, votou-se favorável ao financiamento privado de pesquisas e laboratórios na Unicamp. A única maneira de combater esse projeto é colocando o aparato das universidades a serviço de uma mobilização massiva das três categorias. Se quiser defender a autonomia universitária, Knóbel deve chamar uma assembleia universitária, liberando de suas atividades estudantes, professores e funcionários, que possibilite uma mobilização unificada e sem punições.

Barrar essa ofensiva de Dória e da extrema direita é fundamental como parte de seguir as lutas para que as decisões dentro da universidade sejam verdadeiramente democráticas. É preciso defender que as decisões não sejam tomadas pelo CONSU, e sim por estudantes, funcionários e professores de acordo com seu peso na realidade. Levantamos a necessidade de uma Estatuinte livre e soberana que dissolva o CONSU e permita a representantes eleitos nos cursos proporcionalmente à composição das categorias decidir o conjunto do projeto de universidade. Chega de debates de Estatuto pelas costas da maioria! Um projeto garanta permanência a todos que precisam, ampliação da moradia, revogação de critério meritocrático das bolsas e defendendo que os terceirizados sejam efetivados sem a necessidade de concurso público.

Também é preciso avançar para radicalizar o acesso e por uma universidade que sirva aos trabalhadores e a população. Defendemos as cotas étnico-raciais junto ao fim do vestibular, a estatização das universidades privadas e o fim dos convênios com as empresas sem indenização, como o convênio com a Vale Assassina. Nosso conhecimento deve servir para pensar planos de reflorestamento, novos projetos de mineração, uma reforma agrária radical que ataque os lucros dos agronegócio e dos monopólios, e a demarcação de terras, em defesa dos povos originários.

**Conformar um polo anti-burocrático e contagiar a classe trabalhadora: um Encontro Estadual de Educação convocado a partir da Unicamp**

A UNE vem desmoralizando a disposição da juventude, canalizando para uma estratégia meramente eleitoral, tendo a UJS apoiado Maia. O PT e PCdoB, estando em centenas de DCEs e CAs pelo país, por um lado, e milhares de sindicatos, por outro, impedem organização dos estudantes pela base em centenas de assembleias e reuniões pelo país, e separam a luta da juventude da classe trabalhadora. Justamente por isso, o fato de nosso DCE e principais CAs serem dirigidos pela Oposição de Esquerda da UNE, com correntes do PSOL e PCB, deve nos fazer refletir o papel que uma verdadeira oposição ao controle burocrático das entidades poderia cumprir contra Bolsonaro.

A Unicamp poderia ser um polo que aglutinasse os que querem lutar contra os ataques de Bolsonaro e Dória, expressando uma voz que exigisse um plano de lutas e denunciasse a política levada pela UNE e centrais sindicais, que estão deixando grandes ataques passarem. Esse não é o papel que atualmente cumprem as entidades estudantis na Unicamp. Por isso, para que outras concepções se expressem e aprofundem a experiência dos estudantes com as distintas ideias no movimento estudantil defendemos um DCE de gestão proporcional.

Além disso, nós, assinantes desta Tese, propomos que do Congresso votemos um grande Encontro Estadual de Educação a partir de SP, chamando as universidades a um polo anti-burocrático. Uma política como essa, que propomos encabeçar junto ao DCE e CAs, demonstraria que nossas entidades não estão apenas seguindo a rotina dos aparatos e das eleições, e sim querem organizar uma força real que faça diferença contra Bolsonaro. Não é hora de diálogos amigáveis com a extrema direita, como faz Freixo com Janaína Paschoal, e sim de radicalizar um projeto que possa apontar uma saída à crise contagiando os trabalhadores com a força da juventude.

Assinam essa tese:

Amanda Cinti Emiliano - IA- 165523 <br>
Flavia Teles dos Santos - IFCH - 155397 <br>
Cássia Rodrigues Silva - IFCH - 159651 <br>
Úrsula Noronha - FE - 206391 <br>
Júlia da Silva Oliveira - FE - 219208 <br>
Luiz Carlos -IFCH- 240438 <br>
Vitória Camargo Baggio - IFCH - 178564 <br>
Juliana Begiato - IFCH - 238383 <br>
Taís Helena Cardoso Roldão - IFCH - 205944 <br>
Matheus Correia dos Reis e Souza - IE 156706 <br>
Isadora de Lima Romera - artes visuais - 199293 <br>
Laura Rossi Baraldi - IFCH - 239030 <br>
Ana Vitoria Cavalcante Santos - IFCH - 212922 <br>
Vinícius de Oliveira Santos Barão - IE - 188131 <br>
Guilherme Zanni - imecc - 157961 <br>
Julia Felice Incao - IG - 044334 <br>
Giovanna Leticia Vasconcellos Isidoro - FE- 255680 <br>
Vinícius Miano- IE - 206842 <br>
Márcio da Costa Ferreira - IFCH - 103323 <br>
Paula Sofia Moraes Vitor - FE - 204385 <br>
Luan Assunção Silva - IA - 156298 <br>
Eduardo Duarte Derisso - IE - 215182 <br>
Bianca Barboza Lino - IFCH - 213688 <br>
Pedro Andrade Ferreira Sobrinho - Ciência da Computacao - 242887 <br>
Isis Santos Caniello - FE - 199325 <br>
Julia Diez - FE - 176857 <br>
Luan Gustavo de Resende - IFCH -172462 <br>
José Eduardo de Oliveira - IFCH - 237988 <br>
Diego Malta da Silva - IFCH- 196108 <br>
Júlia Ortega Simões de Almeida - IFCH - 177030 <br>
João Vitor Paula Souza Luz - FCM - 199897 <br>
Mateus Vicente - IFCH - 203318 <br>
Isabella Claro Tegon - FE - 175267 <br>
Eduardo Moric Vilela Mariano - IA - 196283 <br>
Victor Bernardes da Silva - IFCH - 157458 <br>
Tharik Zaidan Maluf - IFCH- 206154 <br>
João Vitor Maia Correia Motta - IE - 155952 <br>
Daniela Paro - IFCH-  214809 <br>
Rodolfo Ventura dos Santos - IA - 158391 <br>
Berno Logis - IFCH - 123746 <br>
Giovanna Coelho  Cornacchia - IFCH - 197977 <br>
Julia Borges Santos - IFCH - 200160 <br>
Erika siste Boaventura - FE - 150495 <br>
Alex Vinícius Rosolen - IFCH - 230570 <br>
Iapona Ferreira Guajajara - IFCH - 260893 <br>
Victor de Souza Cuba - IFCH - 245017 <br>
Vanessa Brito Passos - Arquitetura e Urbanismo - 206421 <br>
Brunno Souza Toledo Pereira - IFCH - 213923 <br>
Matheus Ribeiro da Silva - IG - 241859 <br>
Klymak junior Francisco dos Santos - FE - 260878 <br>
Brenda de Freitas - IFCH - 232190 <br>
André Martho Domingues  - IFCH - 213051 <br>
Ricardo Ferreira Matiazzo - IFCH - 243882 <br>
Guilherme Augusto Vasconcellos Isidoro -IFCH - 217120 <br>
Claudemir Padilha Junior - FE - 159656 <br>
Ana Luiza Vieira Kehdi - IEL - 193979 <br>
Waldir Junio Rocha Rodrigues -IFCH- 245750 <br>
Gustavo Valdivino da Silva - IE - 169444 <br>
Alvaro Henrique Jesus Machado - IFCH - 212463 <br>
Matheus Charão de Carvalho - IFCH - 241676 <br>
Fernando Luchiari - IFCH - 196983 <br>
Guilherme Anhê Lopes Teixeira - IFCH - 217107 <br>
