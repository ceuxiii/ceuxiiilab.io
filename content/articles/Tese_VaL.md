title: Tese Vamos à Luta e estudantes independentes
category: teses
show: yes
date: 2019-10-1
author: CO

---

**Tese ao XIII Congresso dos Estudantes da Unicamp** <br>
**Juventude Vamos à Luta e estudantes independentes**

Há mais de uma década de crise econômica mundial, a solução dos governos capitalistas é a mesma: fazer os trabalhadores e juventude pagarem a conta. No país, a extrema direita de Bolsonaro e Mourão, com práticas corruptas que tanto criticaram para se eleger, bombardeia nossa condição de vida aplicando ajustes como a Reforma da Previdência e privatizações.

Quando o governo anunciou cortes de 30% nas Federais, as universidades foram tomadas por assembleias. Sendo parte da atual gestão do DCE, apostamos nas passagens em sala, jograis e panfletagens chamando à mobilização, e os estudantes responderam: no dia 15 de maio, lotamos as ruas de Campinas e, unindo-nos a Adunicamp, STU e CAs, levamos 8 ônibus para São Paulo. Com uma plenária dos movimentos da região, construímos um grande ato no 30 de maio. Em seguida, estudantes e trabalhadores se uniram no 14 de junho, cruzaram os braços e protestaram contra a Reforma da Previdência, demonstrando que havia disposição para unificar as lutas. O 13 de agosto, mesmo menor que os atos de maio, teve 5 mil pessoas na cidade e cursos paralisados.

A popularidade de Bolsonaro vem despencando em escândalos, de Queiroz à #VazaJato, demonstrando que uma parcela crescente, até dos que votaram em sua candidatura, não está disposta a se entregar. Por isso é vital construir um calendário unificado, como o 2 e 3 de outubro. É possível derrotar o governo!

**Não ao Future-se e à Privatização da Universidade - Defender a pesquisa e a ciência!**<br>
Seguindo a agenda neoliberal, o MEC de Weintraub anunciou o programa “Future-se”. Diante de cortes bilionários que ameaçam fechar muitas Federais até o fim do semestre, a "solução" do governo para as universidades é entregar a gestão delas para o capital privado. É um ataque sem precedente. O projeto ameaça a autonomia universitária, a democracia interna, o tripé ensino-pesquisa-extensão e abre a possibilidade de cobrança de mensalidade, permitindo também a contratação de técnicos e professores por "Organizações Sociais" - empresas privadas escolhidas pelo governo -, sem concurso.

O Future-se está apenas na esfera federal, mas não há dúvida que o governador João Dória importará o modelo para as Estaduais Paulistas. Hoje já temos cobrança de mensalidades em extensões da Unicamp e financiamento privado de pesquisas. O IC visa abrir um novo prédio em parceria com grandes empresas, e na FE foi proposto convênio com o Unibanco para o projeto de extensão "Aluno do Futuro". Esse foi barrado, mas novos projetos virão: há anos empresas e bancos tentam pôr as garras na educação.

Bolsonaro é anticiência e defende uma educação acrítica. O fomento à pesquisa está em colapso: o CNPq não poderá pagar as bolsas dos pesquisadores que, na Unicamp, são 479 mestrandos, 685 doutorandos, 54 pós-doutorandos e 742 bolsistas de iniciação científica; a CAPES não fornecerá novas bolsas, e quem começaria a receber ficou sem! Frente essa situação, a reitoria anunciou uma bolsa emergência para quem foi cortado. Enquanto isso a CPI das Universidades, de Dória, se ocupa em "investigar a doutrinação", censurar a pesquisa e questionar a autonomia universitária.

Na saúde e segurança, o plano ultraconservador bolsonarista é privatizar o SUS, mais guerra às drogas e contrarreforma na saúde mental. Faz mais cortes às áreas sociais; apresenta projetos como a "lei anticrime", de Sergio Moro; persegue minorias, sobretudo transsexuais; e investe dinheiro público na
internação compulsória de dependentes químicos em "comunidades terapêuticas" de pastores extremistas. Na Unicamp, a juventude que depende da saúde pública sofre com as exigências acadêmicas enquanto encara campi inseguros e um sistema insuficiente de atenção psicossocial. Todo dia no grupo da Unicamp tem um caso de assédio e machismo. A guarda dos campi devia ter um treinamento por uma segurança humanizada, que substitua a atual ação violenta contra as mulheres, negras e LGBTs. Nos são negados espaços de vivência, sendo proibidas festas no campus. Retomar o espaço público do campus com festas, saraus, palestras e atividades é nossa tarefa, que deve ser feita coletivamente com todo o movimento estudantil.

**BolsoDória: tirem as mãos da Unicamp**<br>
Em 2016 fizemos uma greve histórica na Unicamp contra os cortes, por permanência e cotas étnico- raciais, pauta conquistada com anos de luta do movimento negro. Com os cortes de R$ 457 milhões nas estaduais entre 2015-16, mais 40 milhões na Unicamp neste ano, a permanência estudantil e a contratação de docentes e técnicos está em cheque. Dória não investe os devidos 9,57% do ICMS para USP, UNESP e Unicamp, o que gera um rombo nas finanças. É uma escolha política do governador, pois destina R$ 20 bilhões por ano a isenções fiscais para agronegócio e megaempresários, além de pagar a dívida pública estadual para os banqueiros.

A reitoria já anunciou que a ampliação da moradia, conquista da greve, não vai sair do papel. O Intercampi foi restringido e teve um turno para Campinas, de noite, cancelado; em Limeira não há moradia e não se fala em expansão. O reitor Knobel, pondo-se a favor da educação e da democracia, ao invés de buscar alternativas privadas de financiamento como o Fundo Patrimonial aprovado pelo CONSU, deveria se somar na luta por mais verbas e encerrar as perseguições institucionais aos estudantes grevistas. Defender a permanência é uma pauta para todo o movimento ter como prioridade para efetivar a ampliação da moradia, por mais bolsas e reajuste indexado à inflação.

Para dar um basta e acabar de vez com os planos antipopulares do governo, o caminho é irmos à luta com os trabalhadores da educação, garis, servidores públicos e do transporte. Prestar solidariedade não só com palavra, mas ações, ao lado dos trabalhadores dos Correios, que batalham contra a privatização. Nos inspiramos no exemplo das Federais, como UFSC e UFF: organizar assembleias de base, marchas e lutas ao lado dos trabalhadores universitários fazendo, na prática, uma ampla unidade que deve chegar mesmo às reitorias. Este Congresso é fundamental para organizarmos a luta da graduação junto à pós e servidores!

**Amazônia em Chamas**<br>
Nas últimas semanas se intensificaram os debates pelo ambiente. As queimadas na Amazônia levaram centenas de milhares de pessoas às ruas nos dias 24 e 25 de agosto. Mas não só a floresta corre perigo: Em 8 meses de governo são quase 300 agrotóxicos liberados, envenenando solo, água e o povo! Ele quis extinguir o Ministério do Meio Ambiente e continua o assassinato e ameaças aos Wajãpi, com agressões aos demais povos originários. Seu projeto é entregar o país ao agronegócio, latifundiários e mineradoras, por isso exonerou o presidente do INPE e persegue IBAMA e ICMBio, que já sofrem com cortes de verba. Enquanto se coloca contra a demarcação de terras, Bolsonaro atiçou o 'dia do fogo' realizado pelos ruralistas. A Greve Internacional do Clima, com adesão de 4 milhões no mundo, aponta o caminho para defender o ambiente: ocupar as ruas!

**Os Estudantes precisam de uma Nova Direção**<br>
Esse momento exige a mais ampla unidade contra Bolsonaro. No entanto, os setores que dirigem a UNE (UJS/PT/LPJ), entidade que devia representar os universitários do país, desfazem a unidade das ruas e assembleias para apostar tudo na institucionalidade, deixando as universidades reféns de um calendário de luta esvaziado – com os 50 dias entre o 13 de agosto e o chamado de greve geral da educação para 2 e 3 de outubro. Defender direitos se torna secundário quando a UJS/PCdoB faz “oposição” apoiando Rodrigo Maia (DEM), o nome de Bolsonaro para a Presidência da Câmara, ou quando os governadores do PT no nordeste articulam para que a Reforma da Previdência não apenas seja aprovada, mas inclua os servidores estaduais e municipais. Pior foi quando, no 30 de maio, a presidente da UNE disse ao Estadão que os atos “não são contrários ou favoráveis ao governo”. A necessidade de construir uma nova direção para o movimento estudantil segue atual, por isso defendemos:

- Unidade nas ruas para derrotar BolsoDória

   Esses governos precisam ser derrotados, e para isso é urgente e necessária a mais ampla unidade nas ruas, com um calendário de lutas contra os ataques à educação, as Reformas, privatizações e em defesa das pautas democráticas, contra o autoritarismo e a perseguição a Glenn e David Miranda. Marielle e Mestre Moa presentes!

- Unidade com a classe trabalhadora

   Para estarmos à altura dos desafios é preciso unir forças com nossos aliados: a classe trabalhadora. É papel da juventude se solidarizar com todas as lutas, greves, protestos e mobilizações das trabalhadoras e trabalhadores em defesa de seus direitos, contra a política de BolsoDória. Patrões e governos não estão do nosso lado; as reitorias, quase nunca; Só quem pode ir até o fim pelos interesse dos estudantes são os trabalhadores.

- Dinheiro para banqueiro não, investimento público sim!

   Todos os dias, mais de R$ 2 bilhões são destinados à dívida pública e ao enriquecimento dos banqueiros! Defendemos que esse dinheiro seja revertido para o SUS, a Educação Pública, um plano de obras públicas para gerar emprego e renda, lazer, esporte e artes para nosso povo, e permanência estudantil nas universidades. Para isso, é preciso revogar a EC95, que congela por 20 anos os investimentos sociais, e deixar de pagar a dívida pública.

- Universalizar o ensino público

   Acesso e permanência não devem ser privilégios. É preciso lutar contra o projeto privatista de BolsoDória e em defesa da educação. Por uma Universidade Pública, Gratuita, de Qualidade e socialmente referenciada, com permanência e valorização dos professores e técnicos-administrativos.

- Primavera feminista, seguir o #ELENÃO

   Nós, mulheres que demos o ponta pé nas lutas contra Bolsonaro em setembro, não podemos parar. Campinas tem taxas de feminicídio 32,5% acima da médica estadual, e não existe Delegacia da Mulher em Barão Geraldo. É necessário lutar contra o assédio, a violência machista e pelo direito ao nosso corpo, com a legalização do aborto.

- Nosso amor é maior que o ódio deles

   Cada 19 horas uma pessoa LGBT+ é assassinada no Brasil. Além disso, apenas 10% das pessoas trans/travestis ocupam postos formais de trabalho, e o presidente não tem vergonha nenhuma de destilar seu ódio. A decisão do STF de criminalizar a LGBTfobia é importantíssima, mas ainda insuficiente. São necessárias políticas públicas para educar toda a população no respeito à diversidade.

- Vidas negras e indígenas importam

   Na greve de 2016 conquistamos as cotas étnico-raciais na Unicamp: uma vitória importantíssima para o movimento estudantil. Bolsonaro, um presidente declaradamente racista e inimigo dos povos originários, atua em defesa dos latifundiários e mineradoras. É preciso combater o racismo estrutural e a falsa política de segurança pública que, de fato, significa o genocídio da população negra; ao mesmo tempo, batalhar pela demarcação de terras e preservação do ambiente.


Assinam essas teses:

Mateus Pereira do Carmo, 174371 <br>
Douglas Sena Rodrigues, 170021 <br>
Christian Queiroz, 169116 <br>
Lucas Felix Dantas Rocha, 136625 <br>
Gabriela Pires Cury, 155510 <br>
Thales Iwashima Andrade, 187462 <br>
Giulia Bombonatti, 173450 <br>
Igor Fernando dos Santos, 217960 <br>
Luís Fernando Silva, 173170 <br>
Brendon Willian Zimermann, 213769 <br>
Sidney de Figueiredo Peres, 224459 <br>
Lucas Croce, 201830 <br>
Arthur Cesar Viana Branco, 157698 <br>
Felipe Emos, 146009 <br>
Cinthia Gil Choi, 195691 <br>
Pedro Andrade Ferreira Sobrinho, 242887 <br>
Matheus Gomes da Silva, 222123 <br>
Dilan Diego Carli dos Santos, 145868 <br>
João Pedro Silva Rodrigues, 226093 <br>
Eduardo Barros Innarelli, 170161 <br>
Leonardo Maldonado Pagnez, 172017 <br>
Maria Eduarda Lemes Baptista, 221648 <br>
Manoel Sousa Ferreira Filho, 202521 <br>
Melissa Real Bispo Lanaro, 174812 <br>
Naomi Pomella Rosenburst, 223492 <br>
Pedro Silva Franco, 175951 <br>
Caio Felipe Romeiro, 195205 <br>
Marina Domingues Landert, 203135 <br>
Giovanna Morena Barbosa Nogueira, 168681 <br>
Maria Gabriela Santana da Silva, 202815 <br>
Gabriel Cosenza Ajauskas, 197232 <br>
Gabriel Gomes dos Santos, 172141 <br>
Manuela Queiroz, 240671 <br>
Larissa Elias, 238842 <br>
Denis dos Santos Alves, 233514 <br>
Maria Eduarda Lemes Baptista, 221648 <br>
Valéria de Souza, 206405 <br>
Manuela Onofre, 203534 <br>
Jefferson Rodrigo Santos de Souza, 175740 <br>
Nicolas Franklin Borges, 185143 <br>
Rodrigo Miranda de Carvalho, 205551 <br>
Daniela Palumbo, 166301 <br>
Natan Rodrigues de Oliveira, 175154 <br>
Antonio Silveira de Moraes, 231569 <br>
Alisson Vinicius Salvador de Lima, 163519 <br>
Vitória Bonuccelli Heringer Lisboa, 188496 <br>
Mariana Hernandes Porto, 183749 <br>
Mariana Nobuko Hayakawa, 203030 <br>
Augusto Piato Amstalden, 213364 <br>
Matheus Alves Silva, 222123 <br>
Danielly Severiano do Norte, 090863 <br>
Aline Romanini da Silva, 145157 <br>
Luana Cristina Santos Marques, 220533 <br>
