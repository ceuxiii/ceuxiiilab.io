title: Tese O ataque à autonomia universitária é preparativo para a privatização das Universidades Brasileiras
category: teses
show: yes
date: 2019-10-1
author: CO

---

__O ataque à autonomia universitária é preparativo para a privatização das Universidades Brasileiras__

A crise de decomposição do capitalismo a nível mundial avança. No Brasil se agudiza a crise do capitalismo burocrático. Ao imperialismo, principalmente estadunidense, se faz necessário expandir sua dominação econômica, política e ideológica sobre o Brasil, aumentando ainda mais nossa subjugação, destruindo o que resta de soberania e saqueando os parcos direitos da classe trabalhadora que foram conquistados com muita luta, em especial as “reformas” e os ataques a educação básica e superior.

O atual governo, por meio da aplicação de seus planos para a educação, tem expressado sua verdadeira face: obscurantista, anticiência e antipovo. Seu plano de destruição e privatização da Universidade pública, como parte da sua tentativa de acabar com a produção científica nacional cumprem a tarefa de pôr todas as Universidades Brasileiras não a serviço do Povo brasileiro, mas sim a serviço do Imperialismo e às grandes empresas multinacionais, assim, o ataque à autonomia universitária é preparativo para a privatização das Universidades Brasileiras. Dada a importância da conquista da gratuidade para a Universidade Brasileira e como os estudantes são atiçados a lutar quando atacada, o governo tem preparado o terreno para destruí-la. Assim, para tentar desarticular as Universidades Brasileiras e aplicar o plano de privatização total, o ataque à Autonomia e Democracia universitárias é decisivo. Projetos como o Future-se! ou ataques como a CPI das universidades são tentativas de destruir nossa frágil autonomia para avançar nos processos de privatização.

Nossa Universidade é produto da luta de classes em nosso país e sua história reflete o nosso processo incompleto de conformação nacional. Diferentemente das colônias espanholas que ainda no século XVII fundaram suas primeiras universidades, no Brasil colônia, os filhos da aristocracia e das classes médias abastadas se dirigiam à Lisboa ou Coimbra para fazer seus estudos universitários. As primeiras faculdades surgiram no Brasil apenas no século XIX, primeira na Bahia a escola de Cirurgia, e depois com os cursos de Direito em Olinda
e São Paulo. Não por coincidência, Salvador e Olinda nos mesmos períodos foram palco de importantes lutas pela conformação nacional, a Revolução Praieira e a Conjuração Baiana.

No século XX, enquanto os ideais do levante de Córdoba, com suas consignas de: caráter público, cátedras livres, gratuidade e co-governo, rapidamente se expandiram para toda América Latina, no Brasil os ecos dessa rebelião chegaram com muito atraso. De uma forma geral, podemos caracterizar a diferença da Universidade Brasileira, no século XX, o atraso em sua formação e em sua democratização, como reflexo do baixo nível de conformação nacional de nosso país. Enquanto na América Espanhola, revoluções nacionais, ainda que limitadas, decidiram com um mínimo de radicalidade o processo de independência nacional, aqui no Brasil nossa independência é proclamada pelo filho do rei de Portugal e nossa república, 60 anos depois, é consagrada por um “Marechal” monarquista. O que tivemos de conquistas, de mínimo de democracia, mesmo que aparente, foi conquistada através da luta combativa do povo brasileiro.

No entanto, o que temos ainda é pouco: uma autonomia extremamente frágil; uma liberdade de cátedra completamente ameaçada por políticas como as CPIs das Universidades federais; seu caráter público bastante precário, pois há uma grande estruturação de capital burocrático nas Universidades, seja através de fundações privadas, seja dos departamentos. A democracia universitária tem muito de farsa: não existe eleição para reitor, apenas uma consulta à comunidade; nos Conselhos Universitários a maior parte dos membros são indicados, diretamente, pelos reitores (os seus pró-reitores, por exemplo, que têm direito à voto), outra parte é eleita pelo voto dos professores; a participação estudantil é minoritária. Talvez, podemos dizer que o eco de Córdoba chega ao Brasil apenas em 1963, quando dirigida pela UNE (União Nacional dos Estudantes) – naquele momento uma entidade de luta - uma greve nacional exigiu a participação de 1/3 dos estudantes nos Conselhos Universitários, isto é, a expressão à época do co-governo (participação estudantil na gerência e decisões sobre a universidade).

Embora a Universidade Brasileira atual não seja nosso modelo, ela deve ser nosso ponto de partida. A gratuidade foi conquistada muito recentemente através de muita luta e os auxílios estudantis ainda não são garantidos por lei; a democracia, embora longe de ser plena, garante o direito a organização e discussão política livre; e a autonomia, a mais frágil de todas, ainda é exercida quando a Universidade delibera sobre seus gastos de forma independente do Estado.

A Universidade Brasileira tem um papel importante de caixa de ressonância das rebeliões populares. O movimento estudantil democrático brasileiro foi linha de frente de importantes lutas do nosso povo em diversos períodos de nossa história: no combate contra o regime militar fascista de 64; lutando contra a implementação da ALCA; contra a farra da Copa e das Olimpíadas; e apoiando as lutas democráticas dos trabalhadores da cidade e a luta pela terra dos camponeses pobres sem terra. A existência de uma Universidade Pública, Gratuita, Autônoma e Democrática é essencial para a continuação dessas lutas. Somente através de uma Universidade realmente democrática, com ampla participação estudantil em suas decisões, será possível a defesa de uma autonomia verdadeira que garanta o direito de ensinar e aprender os acúmulos históricos da humanidade, o pensamento crítico e a luta democrática e a manutenção da Universidade gratuita.

**Nossa tática de luta!**

Apenas através da organização do movimento estudantil independente, classista e combativo que derrube os muros da Universidade e se ligue às massa de nosso povo é que nossa luta pode progredir.

Precisamos nos organizar e a partir dos exemplos da luta mais recente do movimento estudantil, nos preparar e desenvolver nossa tática para enfrentar a situação de fechamento das Universidades. O Ocupa Bandejão da UERJ foi um processo de luta em que se ocupou o bandejão, que há mais de um ano estava parado, e, através da organização estudantil independente, garantiu a alimentação para os estudantes do campus. Além de garantir a alimentação da comunidade acadêmica, a ocupação do bandejão foi capaz de mobilizar, organizar e politizar estudantes, professores e funcionários unidos pela educação pública. Entretanto, o papel da ocupação não se restringiu somente aos muros da Universidade. Através de panfletagens, manifestações e mobilizações os estudantes colocaram o restaurante universitário também a serviço da população pobre que mora no entorno da UERJ, especialmente os jovens da favela da Mangueira, criando força e unidade entre a comunidade acadêmica e a população.

Assim como no caso da UERJ, o histórico recente da luta dos estudantes está repleto de exemplos de processos que caminham no sentido do co-governo estudantil. Em 2005, os estudantes de Pedagogia da UFPR conduziram uma greve combativa para garantir a implementação do currículo do Pedagogo Unitário, currículo este que foi formulado pelos próprios estudantes com a participação de professores democráticos. Em 2011, ocorreu a ocupação da Universidade Federal de Rondônia que conseguiu pela primeira vez na história do país destituir um Reitor e ter todas as suas pautas atendidas, tamanha a combatividade e persistência nesta luta histórica que durou um mês e contou com o apoio de toda a população de Rondônia.

Essas experiências mostram como que os direitos conquistados dentro da Universidade estão sempre ameaçados, principalmente em momentos de crise, porque não há real autonomia nem democracia. A única forma de garantia desses direitos se dá através da luta dos estudantes e exercendo sua democracia, ou seja, praticando o co-governo estudantil.

Dessa forma, corresponde defender a participação de 50% dos estudantes nos conselhos deliberativos e 50% trabalhadores da educação (professores e funcionários) e eleições diretas para a reitoria. Tudo isso deve ser feito em torno da bandeira de democratização da Universidade que, essencialmente, significa o co-governo estudantil na Universidade Brasileira. Essa é a única forma de defesa real da autonomia universitária. Devemos ter claro que não podemos cair em nenhuma chantagem em relação às perdas dos direitos conquistados. No caso de ameaçarem fechar a Universidade e nos colocarem a “escolha” entre pagar ou fechar, deveremos responder de forma contundente: não iremos pagar, nem iremos deixar fechar. Nós assumiremos a Universidade e exerceremos o co-governo. A Universidade atual é o nosso ponto de partida, mas não o nosso modelo. Podemos perceber, com as experiências destacadas, que é possível avançar no processo de democratização das Universidades Brasileiras, porém somente através da luta classista, combativa e independente para assim garantir uma Universidade pública, gratuita e com autonomia universitária, através da imposição do co-governo estudantil!

**VIVA O MOVIMENTO ESTUDANTIL INDEPENDENTE, CLASSISTA E COMBATIVO!**

Júlia Zanini, 177147 <br>
Marina Oliveira Pinheiro de Faria, 183973 <br>
Daniel Yuri Akiyama, 195888 <br>
Henrique Winnischofer, 157990 <br>
Lucas Dias Pires Gonçalves, 239920 <br>
Bruno Gusmão Ferreira, 150896 <br>
Agatha Lorena De Paulo, 164999 <br>
Daniel Henrique Roela Luciano, 169451 <br>
Gustavo António Nieri de Barros, 174014 <br>
Pedro Gargano Longue, 185646 <br>
Bruna Victor Monteiro, 165113 <br>
Matheus Mantovani dos Santos, 174599 <br>
Ana Paula Cussolim, 193994 <br>
Fernanda Borges de Souza, 196888 <br>
Bruno Eduardo Santos de Oliveira, 165215 <br>
Vinícius do Carmo Melício, 188142 <br>
Alan Rodrigo da Silva Davide, 193406 <br>
César Augusto Denadai Zaragoza, 214536 <br>
Gabrieli Novak Gomes, 235314 <br>
Pedro Dominguez Branco, 223264 <br>
Isabelly Robles da Silva, 237121 <br>
Marina Reixach Ramires, 203172 <br>
