title: Como funciona?
author: Comissão Organizadora
date: 5-09-3019
category: sobre
show: yes

---

**Estrutura**<br>
O CEU é constituído por mesas e grupos de discussão (GDs). Nas mesas, os palestrantes expõem suas visões, experiências e conhecimento sobre o determinado tópico a ser discutido. Esse é um espaço para fomentar o debate e para colocações ou dúvidas dos estudantes. Por sua vez, nos GDs participam apenas estudantes, que se aprofundam nas discussões e tiram resoluções sobre os temas tratados durante as mesas. Há relatores que sistematizam as discussões dos GDs e encaminham relatórios com as propostas levantadas para a Comissão Organizadora. Cada delegado do campus de Campinas deve participar de, no mínimo, 3 (três) atividades do XIII CEU para poder votar na Plenária Final. Para os delegados dos campi de Limeira e de Piracicaba, a presença mínima é de 1 (uma) atividade.

Além das mesas e grupos de discussão acontecerão também oficinas, isto é, espaços de atividades organizados pela comunidade.

**As Teses**<br>
As teses são os documentos que condensam o que coletivos de estudantes pensam ser a política a ser levada ao Congresso, ou seja, suas leituras de conjuntura, programas para a universidade e, no geral propostas para o movimento estudantil. Segundo o regimento do congresso:

_Das Teses e Contribuições ao Congresso_

Art. 14o Serão publicadas, em anexo ao Caderno de Textos do XII Congresso dos Estudantes da UNICAMP, as Teses e, havendo espaço, as Contribuições de grupos de estudantes, respeitados os termos deste regimento.

Parágrafo Único: Havendo espaço para publicação das Contribuições, priorizar-se-á aquelas com maior número de assinaturas. A versão digital publicada no site do XII CEU conterá todas as teses e contribuições.

Art. 15o As Teses deverão ser inscritas através do site do XII CEU até o dia 22 de setembro de 2019.
Parágrafo Único: São pré-requisitos para inscrição de Tese:
I — Ser assinada por, no mínimo, 20 (vinte) estudantes de graduação regularmente matriculados.
II — Ter no máximo 11.000 caracteres (incluindo espaços). Os caracteres que ultrapassarem esse limite serão excluídos.
Art. 16o As Contribuições deverão ser inscritas através do site do XII CEU até o dia 22 de setembro de 2019.
Parágrafo Único: São pré-requisitos para inscrição de Contribuição:
I — Ser assinada por, no mínimo, 1 (um) estudante de graduação regularmente matriculado.
II — Ter no máximo 3.500 caracteres (incluindo espaços). Os caracteres que ultrapassarem esse limite serão excluídos.
