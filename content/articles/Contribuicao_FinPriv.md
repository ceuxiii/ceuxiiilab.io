title: Financiamento privado e dois casos didáticos
category: contribuicoes
show: yes
date: 2019-10-3
author: CO

---

A escolha dos governos federais e estaduais tem sido desidratar os investimentos até que, com fome sede e de joelhos, as universidades “optem” pela grana privada. Gostaria de socializar um fato concreto que abalou o financiamento privado da Educação e Pesquisa num país campeão em capitalismo: os EUA.

**MIT Media Lab, um modelo a se inspirar(?)**<br>
O MIT é um expoente mundial. Só no seu Media Lab há pesquisa em inteligência artificial, neurologia, agricultura e satélites. Cérebros fugidios sonham com esse lugar.

Seu site reporta verbas de US$ 80 milhões de empresas, de fundações a laboratórios específicos e de um fundo anonimizado “soberano”. Nele, se poderia receber as “benesses” filantrópicas separando o interesse acadêmico do privado. Uma ideia defendida por aqui, e que inclusive se encaixa no Future-se.

**Máscaras ao vento** <br>
Sem se descolar do exemplo do #MeToo, jovens mulheres denunciaram Jeffrey Epstein, operador de fundos de pesquisa de alta tecnologia, por manter tráfico internacional de menores e explorá-las sexualmente. O livrinho preto de Epstein com 92 páginas de contatos, divulgado em 2015 pelo Gawker, incluia Trump, Clinton, ex governadores, atores, outros bilionários da tecnologia e uma seção chamada “massagem”, com telefones de vítimas. Todos se distanciaram publicamente de Epstein, que foi preso neste ano e cometeu suicídio em seguida.

Se é difícil a situação se tornar mais execrável, faltava complicar. Em setembro, o New Yorker obteve dezenas de emails provando que o MIT, mesmo banindo Epstein após sua primeira condenação em 2008, usava o fundo anônimo para receber dinheiro dele e o consultava diretamente e com frequência sobre como usar os investimentos, incluindo US$ 2 milhões de Bill Gates. O diretor do Media Lab, que estimulava os funcionários a esconder as doações referindo-se a Epstein como “aquele que não deve ser nomeado”, deixou o cargo.

Os investimentos gerenciados pelo asqueroso homem de US$ 500 milhões se estendiam também a Harvard, Stanford, e aonde mais o dinheiro tiver sido “anonimizado”.

**Nem precisava ir longe** <br>
Foi ainda em 2011, em anos Dilma, criada a EBSERH – Empresa Brasileira de Serviços Hospitalares, para a qual foram terceirizados todos os hospitais de Universidades Federais no país. Diz seu site que é uma “estatal 100% dependente da União, [...] no âmbito do SUS, observando a autonomia universitária”. Dispensa dizer que houve muita luta contra esse plano. De fato, bastou sua implementação para que, como nos contam estudantes da UFMG, os laboratórios de pesquisa hospitalar passassem de pesquisa em cura do câncer a cosméticos da Natura.

**Conclusões** <br>
O caso de Epstein e do MIT é instrumental para provar a indissociabilidade do interesse privado no investimento privado; também é anedótico para expor quão execráveis são alguns dos maiores “manda- chuvas” da pesquisa acadêmica, sendo uma vitória que isso seja posto a público.

Já a EBSERH expõe o quanto discursos políticos são dobrados por dirigentes em nome de manter uma “calmaria” na calamidade da privatização. Também é com duplo discurso que a reitoria, enquanto chama uma grande assembleia em defesa da universidade pública, aprovou no CONSU um Fundo Patrimonial que abre a porteira pra ingerência de empresas aqui dentro.

Contra as garras sujas de uns e apesar dos duplos discursos de outros, é por essas e outras que devemos nos manter intransigentes na luta pelo financiamento e caráter públicos de nossas universidades.
