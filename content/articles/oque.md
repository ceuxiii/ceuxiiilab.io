title: O Que é o Ceu?
author: Comissão Organizadora
date: 5-09-3019
category: sobre
show: yes

---
![O QUE E](/theme/o_que_e.png)

O Congresso dos Estudantes da Unicamp é um dos maiores e mais importantes espaços de discussão e deliberação dos estudantes. Aberto a todos e todas as estudantes da UNICAMP, no CEU debatemos desde questões enfrentadas pela juventude mundo afora até o que compete à dinâmica na nossa universidade. Queremos debater a luta em defesa da educação, em defesa da saúde, do meio ambiente, das liberdades democráticas e armar o conjunto dos estudantes, que se fez ouvir no 15M, no 30M e no 13S para construir uma luta de fôlego consequente e que dê conta de defender o nosso futuro. Assim,queremos que o congresso seja o lugar para avaliar com cuidado nossa atuação nos últimos anos e decidirmos as orientações e rumos das movimentações estudantis pelos próximos dois anos.

Concretamente, o evento é composto por seis mesas, grupos de discussão, oficinas e uma plenária final, onde os delegados votam resoluções para o movimento estudantil a partir dos acúmulos dos espaços e das propostas de teses.Esse é o espaço também para repensar e recriar o movimento estudantil na UNICAMP a partir das contribuições de quem participa do dia a dia das entidades, de quem se afastou e de quem nunca militou no movimento estudantil, é o espaço para colocar críticas, reformular métodos, políticas, programas e preparar os estudantes para defender a educação e o nosso futuro contra os avanços da extrema-direita. PARTICIPE!
