title: Sobre

---

![O QUE E](/theme/o_que_e.png)

# O que é o CEU?

O Congresso de Estudantes (CEU) é um dos órgãos deliberativos mais importantes da categoria discente da UNICAMP. Nele é possível alterar o estatuto do Diretório Central dos Estudantes (DCE), a entidade de representação máxima dos estudantes. O evento é composto por mesas e grupos de discussão (GDs), visando debater e definir os rumos do movimento estudantil da universidade. Qualquer estudante pode participar com direito a voz e os delegados, que são eleitos pelas entidades de base, têm direito a voz e voto. O CEU também se trata de um momento de troca e vivência entre os estudantes da universidade, não só nas mesas e nos GDs, mas também através de oficinas e eventos culturais que ocorrem durante os dias do congresso.

##Como funciona?
O CEU é constituído por mesas e grupos de discussão (GDs). Nas mesas, os palestrantes expõem suas visões, experiências e conhecimento sobre o determinado tópico a ser discutido. Esse é um espaço para fomentar o debate e para colocações ou dúvidas dos estudantes. Por sua vez, nos GDs participam apenas estudantes, que se aprofundam nas discussões e tiram resoluções sobre os temas tratados durante as mesas. Há relatores que sistematizam as discussões dos GDs e encaminham relatórios com as propostas levantadas para a Comissão Organizadora. Cada delegado do campus de Campinas deve participar de, no mínimo, 3 (três) atividades do XIII CEU para poder votar na Plenária Final. Para os delegados dos campi de Limeira e de Piracicaba, a presença mínima é de 1 (uma) atividade.

##O que é um delegado?
Os delegados são estudantes que as Unidades escolhem para serem representantes durante o CEU. Cada instituto/faculdade tem direito a 1 (um) delegado para cada 20 (vinte) estudantes regularmente matriculados na graduação. Se houver mais interessados em ser delegados do que vagas disponíveis, a Unidade precisa realizar um processo de escolha de delegados. Essa escolha pode ser feita de três formas: eleição em assembleia, eleição em urnas ou por indicação referendada em reunião aberta e amplamente divulgada pelo Centro Acadêmico. Caso o seu Centro Acadêmico não esteja divulgando ou construindo o processo de escolha de delegados, converse com a gestão do DCE ou com a Comissão Organizadora, que faremos o possível para ajudá-los a encaminhar isso.
